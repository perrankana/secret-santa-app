package com.pandiandcode.perrankana.esecretsantapp.ui.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Perrankana on 06/12/15.
 */
public class EmptyViewHolder extends RecyclerView.ViewHolder {

    public EmptyViewHolder(View itemView) {
        super(itemView);
    }
}
