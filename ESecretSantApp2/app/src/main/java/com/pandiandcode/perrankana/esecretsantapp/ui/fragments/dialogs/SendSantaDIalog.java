package com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.pandiandcode.perrankana.esecretsantapp.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Perrankana on 29/11/15.
 */
public class SendSantaDialog extends DialogFragment{

    public static final String TAG = SendSantaDialog.class.getSimpleName();

    @Bind(R.id.santa_question) TextView mQuestion;
    @Bind(R.id.email_content) EditText mEmailContent;
    @Bind(R.id.save_santa) ImageButton mOkButton;
    @Bind(R.id.remove_santa) ImageButton mCancelButton;

    private Listener mListener;

    public interface Listener{
        public void onSend(String emailContent);
    }

    public static SendSantaDialog newInstance() {
        return new SendSantaDialog();
    }

    // Build AlertDialog using AlertDialog.Builder
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        android.app.AlertDialog.Builder mDialog = new android.app.AlertDialog.Builder(getActivity());

        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_send_santa, null);
        ButterKnife.bind(this, dialogView);

        mOkButton.setOnClickListener(mOnOkListener);
        mCancelButton.setOnClickListener(mOnCancelListener);

        mDialog.setView(dialogView);
        return mDialog.create();
    }

    public void setListener(Listener listener){
        mListener = listener;
    }

    View.OnClickListener mOnOkListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            if(mEmailContent.getVisibility() == View.GONE){
                mEmailContent.setVisibility(View.VISIBLE);
                mQuestion.setVisibility(View.GONE);
                mEmailContent.setFocusable(true);
                mEmailContent.setFocusableInTouchMode(true);
                mEmailContent.requestFocus();
            }else{
                if(mListener!= null){
                    mListener.onSend(mEmailContent.getText().toString());
                }
                dismiss();
            }
        }
    };

    View.OnClickListener mOnCancelListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            dismiss();
        }
    };
}
