package com.pandiandcode.perrankana.esecretsantapp.model.mediator.webdata;

import com.pandiandcode.perrankana.esecretsantapp.model.DataRequest;
import com.pandiandcode.perrankana.esecretsantapp.model.DataResponse;

import retrofit.http.Body;
import retrofit.http.POST;

/**
 * Created by Perrankana on 28/11/15.
 */
public interface ServiceProxy {

    public static final String SEND_EMAILS_V2 = "/admin-ajax.php?action=ssa_send_emails_v2";

    public static final String SEND_EMAILS = "/admin-ajax.php?action=ssa_send_emails_v3";

    @POST(ServiceProxy.SEND_EMAILS)
    public DataResponse sendEmails(@Body DataRequest dataRequest);
}
