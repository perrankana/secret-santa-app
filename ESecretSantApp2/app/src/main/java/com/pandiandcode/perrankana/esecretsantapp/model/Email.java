package com.pandiandcode.perrankana.esecretsantapp.model;

/**
 * Created by Perrankana on 28/11/15.
 */
public class Email {
    String name;
    String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
