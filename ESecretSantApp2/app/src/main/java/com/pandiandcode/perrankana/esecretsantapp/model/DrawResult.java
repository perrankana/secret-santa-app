package com.pandiandcode.perrankana.esecretsantapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pandiandcode.perrankana.esecretsantapp.persistance.DataBaseOpenHelper;

import java.util.ArrayList;

/**
 * Created by Rocio Ortega on 04/12/2016.
 */

public class DrawResult {

    private int id;
    private int groupId;
    private int secretSantaId;
    private int targetId;

    public DrawResult(int groupId, int secretSantaId, int targetId) {
        this.groupId = groupId;
        this.secretSantaId = secretSantaId;
        this.targetId = targetId;
    }

    public DrawResult(int id, int groupId, int secretSantaId, int targetId) {
        this.id = id;
        this.groupId = groupId;
        this.secretSantaId = secretSantaId;
        this.targetId = targetId;
    }

    public int getId() {
        return id;
    }

    public int getGroupId() {
        return groupId;
    }

    public int getSecretSantaId() {
        return secretSantaId;
    }

    public int getTargetId() {
        return targetId;
    }

    @Override public String toString() {
        return "DrawResult{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", secretSantaId=" + secretSantaId +
                ", targetId=" + targetId +
                '}';
    }

    public static DrawResult load(Context context, int id) {
        DrawResult drawResult = null;
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_DRAWS,
                DataBaseOpenHelper.drawResultsColumns, DataBaseOpenHelper.DRAWS_ID + "=?",
                new String[]{id + ""}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    drawResult = new DrawResult(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return drawResult;
    }

    public static DrawResult loadBySecretSanta(Context context, int secretSantaId) {
        DrawResult drawResult = null;
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_DRAWS,
                DataBaseOpenHelper.drawResultsColumns, DataBaseOpenHelper.DRAWS_SECRETSANTA + "=?",
                new String[]{secretSantaId + ""}, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    drawResult = new DrawResult(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return drawResult;
    }

    public static ArrayList<DrawResult> loadByGroup(Context context, int groupId) {
        ArrayList<DrawResult> drawResults = new ArrayList<>();
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_DRAWS,
                DataBaseOpenHelper.drawResultsColumns, DataBaseOpenHelper.DRAWS_GROUP + "=?", new String[]{groupId + ""},
                null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    drawResults.add(new DrawResult(cursor.getInt(0), cursor.getInt(1), cursor.getInt(2), cursor.getInt(3)));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return drawResults;
    }

    public static ArrayList<SecretSanta> loadSecretSantas(Context context, int groupId) {
        ArrayList<DrawResult> drawResults = loadByGroup(context, groupId);
        ArrayList<SecretSanta> secretSantas = new ArrayList<>();
        for (int i = 0; i < drawResults.size(); i++) {
            if (i == 0) {
                secretSantas.add(SecretSanta.load(context, drawResults.get(i).getSecretSantaId()));
            }
            if (i != drawResults.size() - 1) {
                secretSantas.add(SecretSanta.load(context, drawResults.get(i).getTargetId()));
            }
        }
        return secretSantas;
    }

    public static boolean delete(Context context, int groupId) {
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().delete(DataBaseOpenHelper.TABLE_DRAWS,
                DataBaseOpenHelper.DRAWS_GROUP + "=?", new String[]{groupId + ""}) > 0);
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean save(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.DRAWS_GROUP, this.groupId);
        values.put(DataBaseOpenHelper.DRAWS_SECRETSANTA, this.secretSantaId);
        values.put(DataBaseOpenHelper.DRAWS_TARGET, this.targetId);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().insert(DataBaseOpenHelper.TABLE_DRAWS, null, values)) > 0;
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public static void save(Context context, ArrayList<DrawResult> drawResults) {
        for (DrawResult drawResult : drawResults) {
            drawResult.save(context);
        }
    }

    public boolean delete(Context context) {
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().delete(DataBaseOpenHelper.TABLE_DRAWS,
                DataBaseOpenHelper.DRAWS_ID + "=?", new String[]{this.id + ""}) > 0);
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean update(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.DRAWS_GROUP, this.groupId);
        values.put(DataBaseOpenHelper.DRAWS_SECRETSANTA, this.secretSantaId);
        values.put(DataBaseOpenHelper.DRAWS_TARGET, this.targetId);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().update(DataBaseOpenHelper.TABLE_DRAWS, values,
                DataBaseOpenHelper.DRAWS_ID + "=?",
                new String[]{this.id + ""}) > 0);

        mDbHelper.getWritableDatabase().close();
        return saved;
    }
}
