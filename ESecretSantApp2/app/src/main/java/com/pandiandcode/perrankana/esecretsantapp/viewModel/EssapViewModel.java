package com.pandiandcode.perrankana.esecretsantapp.viewModel;

import android.databinding.BaseObservable;

import com.pandiandcode.perrankana.esecretsantapp.model.SecretSanta;
import com.pandiandcode.perrankana.esecretsantapp.utilities.AvatarUtility;

/**
 * Created by Rocio Ortega on 15/10/2016.
 */

public class EssapViewModel extends BaseObservable {

    private SecretSanta secretSanta;

    public EssapViewModel(SecretSanta secretSanta) {
        this.secretSanta = secretSanta;
    }

    public String getName() {
        return secretSanta.getName();
    }

    public String getEmail() {
        return secretSanta.getEmail();
    }

    public int getAvatar() {
        return AvatarUtility.getRAvatarID(secretSanta.getAvatar());
    }
}
