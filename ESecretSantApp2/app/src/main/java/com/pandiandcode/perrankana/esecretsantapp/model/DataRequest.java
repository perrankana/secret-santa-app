package com.pandiandcode.perrankana.esecretsantapp.model;

import java.util.List;

/**
 * Created by Perrankana on 28/11/15.
 */
public class DataRequest {

    private String lang;
    private String content;
    private List<SecretSanta> emailslist;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<SecretSanta> getEmailslist() {
        return emailslist;
    }

    public void setEmailslist(List<SecretSanta> emailslist) {
        this.emailslist = emailslist;
    }
}
