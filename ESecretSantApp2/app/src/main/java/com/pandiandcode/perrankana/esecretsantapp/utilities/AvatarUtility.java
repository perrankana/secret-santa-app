package com.pandiandcode.perrankana.esecretsantapp.utilities;

import com.pandiandcode.perrankana.esecretsantapp.R;

/**
 * Created by Perrankana on 29/11/15.
 */
public class AvatarUtility {

    public static int getRAvatarID(int id){
        switch(id){
            case 0:
                return R.drawable.avatar1;
            case 1:
                return R.drawable.avatar2;
            case 2:
                return R.drawable.avatar3;
            case 3:
                return R.drawable.avatar4;
            default:
                return R.drawable.avatar1;
        }
    }
}
