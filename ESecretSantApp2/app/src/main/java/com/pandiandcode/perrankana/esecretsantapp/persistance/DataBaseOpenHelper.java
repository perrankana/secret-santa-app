package com.pandiandcode.perrankana.esecretsantapp.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Perrankana on 03/12/14.
 */
public class DataBaseOpenHelper extends SQLiteOpenHelper {

    public static final String TABLE_SECRETSANTA = "secretsantas";
    public static final String SECRETSANTA_NAME = "name";
    public static final String SECRETSANTA_EMAIL = "email";
    public static final String SECRETSANTA_AVATAR = "avatar";
    public static final String SECRETSANTA_ID = "_id";
    public static final String SECRETSANTA_GROUP = "group_id";

    public static final String TABLE_GROUP = "groups";
    public static final String GROUP_ID = "_id";
    public static final String GROUP_NAME = "name";

    public static final String TABLE_DRAWS = "draw_results";
    public static final String DRAWS_ID = "_id";
    public static final String DRAWS_GROUP = "group_id";
    public static final String DRAWS_SECRETSANTA = "secret_santa_id";
    public static final String DRAWS_TARGET = "target_id";

    public static final String[] secretsantaColumns = {SECRETSANTA_ID, SECRETSANTA_NAME, SECRETSANTA_EMAIL,
            SECRETSANTA_AVATAR, SECRETSANTA_GROUP};

    public static final String[] groupColumns = {GROUP_ID, GROUP_NAME};

    public static final String[] drawResultsColumns = {DRAWS_ID, DRAWS_GROUP, DRAWS_SECRETSANTA, DRAWS_TARGET};

    private static final String CREATE_CMD =

            "CREATE TABLE " + TABLE_SECRETSANTA + " (" + SECRETSANTA_ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + SECRETSANTA_NAME + " TEXT NOT NULL, "
                    + SECRETSANTA_EMAIL + " TEXT NOT NULL, "
                    + SECRETSANTA_AVATAR + " INTEGER, "
                    + SECRETSANTA_GROUP + " INTEGER )";

    private static final String CREATE_GROUP_TABLE =

            "CREATE TABLE " + TABLE_GROUP + " (" + GROUP_ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + GROUP_NAME + " TEXT NOT NULL )";

    private static final String CREATE_DRAW_RESULTS =

            "CREATE TABLE " + TABLE_DRAWS + " (" + DRAWS_ID
                    + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + DRAWS_GROUP + " INTEGER, "
                    + DRAWS_SECRETSANTA + " INTEGER, "
                    + DRAWS_TARGET + " INTEGER )";

    private static final String ALTER_SECRETSANTA_TABLE = "ALTER TABLE \n" + TABLE_SECRETSANTA +
            " ADD " + SECRETSANTA_GROUP + " INTEGER ";

    private static final String NAME = "essapp_db";
    private static final Integer VERSION = 2;
    private final Context mContext;

    public DataBaseOpenHelper(Context context) {
        super(context, NAME, null, VERSION);
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_CMD);
        db.execSQL(CREATE_GROUP_TABLE);
        db.execSQL(CREATE_DRAW_RESULTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if (oldVersion == 1 && newVersion == VERSION) {
            db.execSQL(ALTER_SECRETSANTA_TABLE);
            db.execSQL(CREATE_GROUP_TABLE);
            db.execSQL(CREATE_DRAW_RESULTS);
            db.execSQL("INSERT INTO " + TABLE_GROUP + " (" + GROUP_ID + ", " + GROUP_NAME + ") \n" +
                    "VALUES (0, 'Group');");
        }
    }

    void deleteDatabase() {
        mContext.deleteDatabase(NAME);
    }
}
