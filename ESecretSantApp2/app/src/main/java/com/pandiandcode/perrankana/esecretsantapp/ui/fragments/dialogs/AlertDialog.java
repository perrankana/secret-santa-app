package com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.utilities.AvatarUtility;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Perrankana on 29/11/15.
 */
public class AlertDialog extends DialogFragment {

    public static final String TAG = AlertDialog.class.getSimpleName();
    public static final String QUESTION = "question";
    public static final String AVATAR_ID = "avatar_id";

    @Bind(R.id.santa_question) TextView mQuestionTextView;
    @Bind(R.id.santa_avatar) ImageView mAvatar;
    @Bind(R.id.save_santa) ImageButton mOkButton;
    @Bind(R.id.remove_santa) ImageButton mCancelButton;

    private String mQuestion;
    private int mAvatarId;
    private AlertDialogListener mListener;

    public interface AlertDialogListener{
        public void onOkClick();
    }

    public static AlertDialog newInstance(String question, int avatarId) {
        AlertDialog dialog = new AlertDialog();
        Bundle args = new Bundle();
        args.putString(QUESTION, question);
        args.putInt(AVATAR_ID, avatarId);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        android.app.AlertDialog.Builder mDialog = new android.app.AlertDialog.Builder(getActivity());

        View dialogView =getActivity().getLayoutInflater().inflate(R.layout.dialog_santa_alert, null);
        ButterKnife.bind(this, dialogView);

        Bundle args = getArguments();
        if(args!=null){
            mQuestion = args.getString(QUESTION);
            mAvatarId = args.getInt(AVATAR_ID);
        }

        mAvatar.setBackgroundResource(AvatarUtility.getRAvatarID(mAvatarId));
        mQuestionTextView.setText(mQuestion);

        mOkButton.setOnClickListener(mOnOkListener);
        mCancelButton.setOnClickListener(mOnCancelListener);

        mDialog.setView(dialogView);
        return mDialog.create();
    }

    public void setListener(AlertDialogListener listener){
        mListener = listener;
    }

    View.OnClickListener mOnOkListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            if(mListener!= null) {
                mListener.onOkClick();
            }
            dismiss();
        }
    };

    View.OnClickListener mOnCancelListener = new View.OnClickListener() {
        public void onClick(final View v){
            dismiss();
        }
    };
}
