package com.pandiandcode.perrankana.esecretsantapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pandiandcode.perrankana.esecretsantapp.persistance.DataBaseOpenHelper;

import java.util.ArrayList;

/**
 * Created by Perrankana on 03/12/14.
 */
public class SecretSanta {
    private int id;
    private String name;
    private int avatar;
    private String email;
    private int groupId;

    public SecretSanta(String name, String email, int avatar, int groupId) {
        this.name = name;
        this.email = email;
        this.avatar = avatar;
        this.groupId = groupId;
    }

    public SecretSanta(int id, String name, String email, int avatar, int groupId) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.avatar = avatar;
        this.groupId = groupId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public int getAvatar() {
        return avatar;
    }

    public int getID() {
        return id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    @Override public String toString() {
        return "SecretSanta{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", avatar=" + avatar +
                ", email='" + email + '\'' +
                ", groupId=" + groupId +
                '}';
    }

    public static SecretSanta load(Context context, int id) {
        SecretSanta secretSanta = null;
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_SECRETSANTA,
                DataBaseOpenHelper.secretsantaColumns, DataBaseOpenHelper.SECRETSANTA_ID + "=?",
                new String[]{id + ""}, null, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    secretSanta = new SecretSanta(cursor.getInt(0), cursor.getString(1), cursor.getString(2), cursor
                            .getInt(3), cursor.getInt(4));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return secretSanta;
    }

    public static ArrayList<SecretSanta> load(Context context) {
        ArrayList<SecretSanta> secretSantas = new ArrayList<>();
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_SECRETSANTA,
                DataBaseOpenHelper.secretsantaColumns, null, new String[]{}, null, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    secretSantas.add(new SecretSanta(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                            cursor.getInt(3), cursor.getInt(4)));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return secretSantas;
    }

    public static ArrayList<SecretSanta> loadByGroup(Context context, int groupId) {
        ArrayList<SecretSanta> secretSantas = new ArrayList<SecretSanta>();
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_SECRETSANTA,
                DataBaseOpenHelper.secretsantaColumns, DataBaseOpenHelper.SECRETSANTA_GROUP + "=?",
                new String[]{groupId + ""}, null, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    secretSantas.add(new SecretSanta(cursor.getInt(0), cursor.getString(1), cursor.getString(2),
                            cursor.getInt(3), cursor.getInt(4)));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return secretSantas;
    }

    public boolean save(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.SECRETSANTA_NAME, this.name);
        values.put(DataBaseOpenHelper.SECRETSANTA_EMAIL, this.email);
        values.put(DataBaseOpenHelper.SECRETSANTA_AVATAR, this.avatar);
        values.put(DataBaseOpenHelper.SECRETSANTA_GROUP, this.groupId);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().insert(DataBaseOpenHelper.TABLE_SECRETSANTA, null, values)) > 0;
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean delete(Context context) {
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().delete(DataBaseOpenHelper.TABLE_SECRETSANTA,
                DataBaseOpenHelper.SECRETSANTA_ID + "=?",
                new String[]{this.id + ""}) > 0);
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean update(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.SECRETSANTA_NAME, this.name);
        values.put(DataBaseOpenHelper.SECRETSANTA_EMAIL, this.email);
        values.put(DataBaseOpenHelper.SECRETSANTA_AVATAR, this.avatar);
        values.put(DataBaseOpenHelper.SECRETSANTA_GROUP, this.groupId);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().update(DataBaseOpenHelper.TABLE_SECRETSANTA, values,
                DataBaseOpenHelper.SECRETSANTA_ID + "=?",
                new String[]{this.id + ""}) > 0);

        mDbHelper.getWritableDatabase().close();
        return saved;
    }
}
