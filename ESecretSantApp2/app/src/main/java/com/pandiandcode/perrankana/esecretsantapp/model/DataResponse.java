package com.pandiandcode.perrankana.esecretsantapp.model;

/**
 * Created by Perrankana on 28/11/15.
 */
public class DataResponse {
    boolean success;

    public boolean getSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

}
