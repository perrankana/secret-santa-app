package com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.utilities.AvatarUtility;

import java.util.Random;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Perrankana on 29/11/15.
 */
public class SantaDialog extends DialogFragment {

    public static final String TAG = SantaDialog.class.getSimpleName();
    public static final String IS_EDIT = "is_edit";
    public static final String NAME = "name";
    public static final String EMAIL = "email";
    public static final String AVATAR_ID = "avatar_id";
    public static final String SANTA_ID = "santa_id";

    @Bind(R.id.santa_name) TextView mNameTextView;
    @Bind(R.id.santa_email) TextView mEmailTextView;
    @Bind(R.id.santa_avatar) ImageView mAvatarImageView;
    @Bind(R.id.save_santa) ImageButton mSaveButton;
    @Bind(R.id.remove_santa) ImageButton mCancelButton;

    private NewSantaDialogListener mListener;
    private int mAvatarId;
    private boolean mIsEdit;
    private String mName;
    private String mEmail;
    private int mSantaId;

    public interface NewSantaDialogListener{
        public void onSaveSanta(int santaId, String name, String email, int avatarId);
    }

    public static SantaDialog newInstance() {
        SantaDialog dialog = new SantaDialog();
        Bundle args = new Bundle();
        args.putBoolean(IS_EDIT, false);
        dialog.setArguments(args);
        return dialog;
    }

    public static SantaDialog newInstance(int santaId, String name, String email, int avatarId) {
        SantaDialog dialog = new SantaDialog();
        Bundle args = new Bundle();
        args.putBoolean(IS_EDIT, true);
        args.putInt(SANTA_ID, santaId);
        args.putString(NAME, name);
        args.putString(EMAIL, email);
        args.putInt(AVATAR_ID, avatarId);
        dialog.setArguments(args);
        return dialog;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        View dialogView =getActivity().getLayoutInflater().inflate(R.layout.dialog_new_santa, null);
        ButterKnife.bind(this, dialogView);

        Bundle args = getArguments();
        if(args!= null){
            mIsEdit = args.getBoolean(IS_EDIT, false);
            if(mIsEdit){
                mSantaId = args.getInt(SANTA_ID);
                mName = args.getString(NAME);
                mEmail = args.getString(EMAIL);
                mAvatarId = args.getInt(AVATAR_ID);
                mNameTextView.setText(mName);
                mEmailTextView.setText(mEmail);
            }else{
                mAvatarId = (new Random()).nextInt(4);
            }
        }
        mAvatarImageView.setBackgroundResource(AvatarUtility.getRAvatarID(mAvatarId));

        mSaveButton.setOnClickListener(mOnOkClickListener);
        mCancelButton.setOnClickListener(mOnCancelClickListener);

        dialog.setView(dialogView);
        return dialog.create();
    }

    public void setListener(NewSantaDialogListener listener){
        mListener = listener;
    }

    private View.OnClickListener mOnOkClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            if(!TextUtils.isEmpty(mNameTextView.getText()) && !TextUtils.isEmpty(mEmailTextView.getText())) {
                if(android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailTextView.getText()).matches()){
                    if(mListener != null) {
                        mListener.onSaveSanta(mSantaId, mNameTextView.getText().toString(), mEmailTextView.getText()
                                .toString(), mAvatarId);
                    }
                    dismiss();

                }
            }
        }
    };

    private View.OnClickListener mOnCancelClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            dismiss();
        }
    };

}
