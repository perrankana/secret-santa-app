package com.pandiandcode.perrankana.esecretsantapp.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs.AlertDialog;

/**
 * Created by Rocio Ortega on 20/11/2016.
 */

public class PermissionUtility {

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    public static boolean hasContactPermission(Context context) {
        return ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED;
    }

    public static void requestPermission(final AppCompatActivity activity) {
        if (!hasContactPermission(activity)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CONTACTS)) {
                AlertDialog dialog = AlertDialog.newInstance(activity.getString(R.string.contact_permission), 1);
                dialog.setListener(new AlertDialog.AlertDialogListener() {
                    @Override public void onOkClick() {
                        ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS},
                                MY_PERMISSIONS_REQUEST_READ_CONTACTS);
                    }
                });
                dialog.show(activity.getSupportFragmentManager(), "request_permission");
            } else {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);
            }
        }
    }

    public static void contactPermissionGranted(int requestCode, @NonNull int[] grantResults, ContactPermissionCalback
            callback) {
        if (requestCode == PermissionUtility.MY_PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (callback != null) {
                    callback.onPermissionGranted();
                }
            } else {
                if (callback != null) {
                    callback.onPermissionDenied();
                }
            }
        }
    }

    public interface ContactPermissionCalback {
        void onPermissionGranted();

        void onPermissionDenied();
    }
}
