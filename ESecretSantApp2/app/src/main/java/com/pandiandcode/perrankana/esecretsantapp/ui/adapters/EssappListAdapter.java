package com.pandiandcode.perrankana.esecretsantapp.ui.adapters;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.model.SecretSanta;
import com.pandiandcode.perrankana.esecretsantapp.ui.viewholders.EmptyViewHolder;
import com.pandiandcode.perrankana.esecretsantapp.ui.viewholders.SantaViewHolder;
import com.pandiandcode.perrankana.esecretsantapp.utilities.AvatarUtility;
import com.pandiandcode.perrankana.esecretsantapp.viewModel.EssapViewModel;

import java.util.ArrayList;

/**
 * Created by Perrankana on 03/12/14.
 */
public class EssappListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final String TAG = EssappListAdapter.class.getSimpleName();
    static final int EMPTY_TYPE = 0;
    static final int ITEM_TYPE = 1;

    private ArrayList<SecretSanta> mList = new ArrayList<>();
    private EssappAdapterListener mListener;

    public interface EssappAdapterListener{
        public boolean onItemLongClick(int position, SecretSanta secretSanta);
        public void onItemClick(int position, SecretSanta secretSanta);
    }

    public EssappListAdapter(ArrayList<SecretSanta> allSantas) {
        mList = new ArrayList<>(allSantas);
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case EMPTY_TYPE:
                return new EmptyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_empty_list, parent, false));

            case ITEM_TYPE:
                final SantaViewHolder holder = new SantaViewHolder(LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.layout_secretsanta_item,parent, false));

                holder.itemView.setLongClickable(true);
                holder.itemView.setTag(holder);
                holder.itemView.setOnClickListener(mOnClickListener);
                holder.itemView.setOnLongClickListener(mOnLongClickListener);
                holder.getToolbar().setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
                    @Override public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()){
                            case R.id.edit_santa:
                                mOnClickListener.onClick(holder.itemView);
                                break;
                            case R.id.delete_santa:
                                mOnLongClickListener.onLongClick(holder.itemView);
                                break;
                        }
                        return false;
                    }
                });
                return holder;
        }
        return null;
    }

    @Override public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()){
            case ITEM_TYPE:
                SecretSanta santa = mList.get(position);
                SantaViewHolder santaHolder = (SantaViewHolder) holder;
                santaHolder.bind(new EssapViewModel(santa));
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override public int getItemCount() {
        if(mList.size() == 0){
            return 1;
        }
        return mList.size();
    }

    @Override public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        mListener = null;
    }

    @Override public int getItemViewType(int position) {
        if(mList.size() == 0){
            return EMPTY_TYPE;
        }else{
            return ITEM_TYPE;
        }
    }

    public void setListener(EssappAdapterListener listener){
        mListener = listener;
    }

    public void add(SecretSanta listItem) {
        mList.add(listItem);
        if(mList.size() == 1){
            notifyItemChanged(1);
        } else {
            notifyItemInserted(getItemCount() - 1);
        }
    }

    public void remove(int position){
        mList.remove(position);
        notifyItemRemoved(position);
    }

    public void update (int position, SecretSanta listItem){
        mList.set(position,listItem);
        notifyItemChanged(position);
    }

    public void update (ArrayList<SecretSanta> list){
        this.mList =list;
        notifyDataSetChanged();
    }

    public ArrayList<SecretSanta> getSantas(){
        return mList;
    }

    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override public void onClick(View v) {
            if(mListener!= null){
                SantaViewHolder holder = (SantaViewHolder)v.getTag();
                SecretSanta secretSanta = mList.get(holder.getAdapterPosition());
                mListener.onItemClick(holder.getAdapterPosition(), secretSanta);
            }
        }
    };

    View.OnLongClickListener mOnLongClickListener = new View.OnLongClickListener() {
        @Override public boolean onLongClick(View v) {
            if(mListener!= null){
                SantaViewHolder holder = (SantaViewHolder)v.getTag();
                SecretSanta secretSanta = mList.get(holder.getAdapterPosition());
                return mListener.onItemLongClick(holder.getAdapterPosition(), secretSanta);
            }
            return false;
        }
    };
}
