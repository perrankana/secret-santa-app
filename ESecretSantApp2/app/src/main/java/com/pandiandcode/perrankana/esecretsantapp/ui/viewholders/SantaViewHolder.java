package com.pandiandcode.perrankana.esecretsantapp.ui.viewholders;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.databinding.LayoutSecretsantaItemBinding;
import com.pandiandcode.perrankana.esecretsantapp.viewModel.EssapViewModel;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Perrankana on 28/11/15.
 */
public class SantaViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.avatar) ImageView santaAvatar;
    @Bind(R.id.name) TextView santaName;
    @Bind(R.id.email) TextView santaEmail;
    @Bind(R.id.toolbar) Toolbar mToolbar;

    private LayoutSecretsantaItemBinding mBinding;

    public SantaViewHolder(View itemView) {
        super(itemView);
        mBinding = DataBindingUtil.bind(itemView);
        ButterKnife.bind(this, itemView);
        mToolbar.inflateMenu(R.menu.menu_santa_item);
    }

    public void bind(EssapViewModel santa) {
        mBinding.setSanta(santa);
    }

    public ImageView getSantaAvatar() {
        return santaAvatar;
    }

    public TextView getSantaName() {
        return santaName;
    }

    public TextView getSantaEmail() {
        return santaEmail;
    }

    public Toolbar getToolbar() {
        return mToolbar;
    }
}
