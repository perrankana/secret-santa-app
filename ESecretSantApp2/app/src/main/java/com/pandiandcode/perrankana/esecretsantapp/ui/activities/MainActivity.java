package com.pandiandcode.perrankana.esecretsantapp.ui.activities;


import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.pandiandcode.perrankana.esecretsantapp.R;
import com.pandiandcode.perrankana.esecretsantapp.model.DrawResult;
import com.pandiandcode.perrankana.esecretsantapp.model.SecretSanta;
import com.pandiandcode.perrankana.esecretsantapp.task.SendEmailsTask;
import com.pandiandcode.perrankana.esecretsantapp.ui.adapters.EssappListAdapter;
import com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs.AlertDialog;
import com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs.SantaDialog;
import com.pandiandcode.perrankana.esecretsantapp.ui.fragments.dialogs.SendSantaDialog;
import com.pandiandcode.perrankana.esecretsantapp.utilities.ContactUtility;
import com.pandiandcode.perrankana.esecretsantapp.utilities.NetworkUtility;
import com.pandiandcode.perrankana.esecretsantapp.utilities.PermissionUtility;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    @Bind(R.id.recycler_view) RecyclerView mRecyclerView;
    @Bind(R.id.coordinator_layout) CoordinatorLayout mCoordinatorLayout;
    @Bind(R.id.toolbar) Toolbar mToolbar;

    private Snackbar mSnackbar;
    private ProgressDialog mProgressDialog;

    private RecyclerView.LayoutManager mLayoutManager;
    private EssappListAdapter mAdapter;

    private ArrayList<SecretSanta> allSantas;
    private int sPosition;

    private BroadcastReceiver mNetworkReceiver;

    @TargetApi(Build.VERSION_CODES.M)
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupToolbar();
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorStatusBar, null));
            } else {
                getWindow().setStatusBarColor(getResources().getColor(R.color.colorStatusBar));
            }
        }

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        allSantas = new ArrayList<>();
        loadAllSantas();
        mAdapter = new EssappListAdapter(allSantas);
        mAdapter.setListener(mAdapterListener);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLongClickable(true);
    }

    @Override protected void onResume() {
        super.onResume();
        if (mNetworkReceiver == null) {
            setupNetworkChangeListener();
        }
    }

    @Override protected void onPause() {
        super.onPause();
        if (mNetworkReceiver != null) {
            unregisterReceiver(mNetworkReceiver);
            mNetworkReceiver = null;
        }
    }

    protected void setupToolbar() {
        setSupportActionBar(mToolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.send_santa:
                if (NetworkUtility.isConnectedToNetwork(this)) {
                    if (mAdapter.getItemCount() > 2) {
                        SendSantaDialog dialog = SendSantaDialog.newInstance();
                        dialog.setListener(mSendSantaListener);
                        dialog.show(getSupportFragmentManager(), SendSantaDialog.TAG);
                    } else {
                        showToast(getString(R.string.not_enough_santas));
                    }
                } else {
                    showNoNetworkSnackbar();
                }
                return true;
            case R.id.re_send_santa:
                // TODO: 04/12/2016 use group Id 
                ArrayList<SecretSanta> secretSantas = DrawResult.loadSecretSantas(getApplicationContext(), 0);
                for (SecretSanta secretSanta : secretSantas) {
                    Log.d(TAG, secretSanta.toString());
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ContactUtility.PICK_CONTACT_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                SecretSanta secretSanta = ContactUtility.getSecretSantaFromContacts(getContentResolver(), data);
                showAddSantaDialog(secretSanta);
            } else {
                showAddSantaDialog(null);
            }
        }
    }

    @Override public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionUtility.contactPermissionGranted(requestCode, grantResults, new PermissionUtility.ContactPermissionCalback() {
            @Override public void onPermissionGranted() {
                startActivityForResult(ContactUtility.createPickContactIntent(), ContactUtility
                        .PICK_CONTACT_REQUEST_CODE);
            }

            @Override public void onPermissionDenied() {
                MainActivity.super.onPostResume();
                showAddSantaDialog(null);
            }
        });
    }

    @OnClick(R.id.add_santa) public void addSanta() {
        if (PermissionUtility.hasContactPermission(this)) {
            startActivityForResult(ContactUtility.createPickContactIntent(), ContactUtility.PICK_CONTACT_REQUEST_CODE);
        } else {
            PermissionUtility.requestPermission(this);
        }
    }

    private void showAddSantaDialog(SecretSanta secretSanta) {
        SantaDialog mDialog;
        if (secretSanta == null) {
            mDialog = SantaDialog.newInstance();
        } else {
            mDialog = SantaDialog.newInstance(secretSanta.getID(), secretSanta.getName(),
                    secretSanta.getEmail(), secretSanta.getAvatar());
        }
        mDialog.setListener(mNewSantaDialogListener);
        mDialog.show(getSupportFragmentManager(), SantaDialog.TAG);
    }

    private EssappListAdapter.EssappAdapterListener mAdapterListener = new EssappListAdapter.EssappAdapterListener() {
        @Override public boolean onItemLongClick(int position, SecretSanta secretSanta) {
            sPosition = position;
            AlertDialog deleteDialog = AlertDialog.newInstance(String.format(getResources().getString(R.string.delete_santa_question),
                    secretSanta.getName()), secretSanta.getAvatar());
            deleteDialog.setListener(mDeleteSantaListener);
            deleteDialog.show(getSupportFragmentManager(), AlertDialog.TAG);
            return true;
        }

        @Override public void onItemClick(int position, SecretSanta secretSanta) {
            sPosition = position;
            showEditSantaDialog(secretSanta);
        }
    };

    private void showEditSantaDialog(SecretSanta secretSanta) {
        SantaDialog dialog = SantaDialog.newInstance(secretSanta.getID(), secretSanta.getName(),
                secretSanta.getEmail(), secretSanta.getAvatar());
        dialog.setListener(mEditSantaDialogListener);
        dialog.show(getSupportFragmentManager(), SantaDialog.TAG);
    }

    public void sendEmail(String emailContent, ArrayList<SecretSanta> secretSantas) {
        new SendEmailsTask(getApplicationContext(), emailContent, secretSantas, new SendEmailsTask.SendEmailsCallback() {
            @Override public void doBefore() {
                showProgressDialog(R.string.sending);
            }

            @Override public void onSuccess() {
                hideProgressDialog();
                AlertDialog rateDialog = AlertDialog.newInstance(getString(R.string.rating), 0);
                rateDialog.setListener(mRateDialogListener);
                rateDialog.show(getSupportFragmentManager(), AlertDialog.TAG);
                showToast(getString(R.string.sent_message));
            }

            @Override public void onFailed() {
                hideProgressDialog();
                showToast(getString(R.string.not_sent_message));
            }
        }).execute();
    }

    public void loadAllSantas() {
        allSantas = SecretSanta.load(this);
    }

    public void showToast(String str_text) {
        LayoutInflater inflater = getLayoutInflater();
        View layout = inflater.inflate(R.layout.layout_custom_toast, (ViewGroup) findViewById(R.id.toast_layout_root));

        TextView text = (TextView) layout.findViewById(R.id.toast_test);
        text.setText(str_text);

        Toast toast = new Toast(getApplicationContext());
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.setDuration(Toast.LENGTH_LONG);
        toast.setView(layout);
        toast.show();
    }

    private void showNoNetworkSnackbar() {
        mSnackbar = Snackbar.make(mCoordinatorLayout, getString(R.string.no_internet), Snackbar
                .LENGTH_INDEFINITE);
        mSnackbar.setAction(R.string.yes, new View.OnClickListener() {
            @Override public void onClick(View v) {

            }
        });
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            ((TextView) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor
                    (getResources().getColor(R.color.windowBackground, null));
        } else {
            ((TextView) mSnackbar.getView().findViewById(android.support.design.R.id.snackbar_text)).setTextColor
                    (getResources().getColor(R.color.windowBackground));
        }
        mSnackbar.show();
    }

    private void hideNoNetworkSnackBar() {
        if (mSnackbar != null) {
            mSnackbar.dismiss();
        }
    }

    protected void showProgressDialog(int messageResourceId) {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setCanceledOnTouchOutside(false);
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.setMessage(getString(messageResourceId));

        mProgressDialog.show();
    }

    protected void hideProgressDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }

    private SantaDialog.NewSantaDialogListener mNewSantaDialogListener = new SantaDialog.NewSantaDialogListener() {
        @Override public void onSaveSanta(int santaId, String name, String email, int avatarId) {
            SecretSanta santa = new SecretSanta(name, email, avatarId, 0);
            santa.save(getApplicationContext());
            mAdapter.add(santa);
        }
    };

    private SantaDialog.NewSantaDialogListener mEditSantaDialogListener = new SantaDialog.NewSantaDialogListener() {
        @Override public void onSaveSanta(int santaId, String name, String email, int avatarId) {
            SecretSanta santa = new SecretSanta(santaId, name, email, avatarId, 0);
            santa.update(getApplicationContext());
            mAdapter.update(sPosition, santa);
        }
    };

    private AlertDialog.AlertDialogListener mDeleteSantaListener = new AlertDialog.AlertDialogListener() {
        @Override public void onOkClick() {
            SecretSanta santa = mAdapter.getSantas().get(sPosition);
            santa.delete(getApplicationContext());
            mAdapter.remove(sPosition);
        }
    };

    private AlertDialog.AlertDialogListener mRateDialogListener = new AlertDialog.AlertDialogListener() {
        @Override public void onOkClick() {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName())));
        }
    };

    SendSantaDialog.Listener mSendSantaListener = new SendSantaDialog.Listener() {
        @Override public void onSend(String emailContent) {
            ArrayList<SecretSanta> secretSantas = getShuffleSecretSanta(mAdapter.getSantas());
//            sendEmail(emailContent, secretSantas);
        }
    };

    private ArrayList<SecretSanta> getShuffleSecretSanta(ArrayList<SecretSanta> secretSantas) {
        DrawResult.delete(getApplicationContext(), 0);// TODO: 04/12/2016 use Group id
        Collections.shuffle(secretSantas);

        ArrayList<DrawResult> drawResults = new ArrayList<>();
        SecretSanta firstSecretSanta = secretSantas.get(0);

        if (secretSantas.size() > 2) {
            for (int i = 0; i < secretSantas.size() - 1; i++) {
                DrawResult drawResult = new DrawResult(secretSantas.get(i).getGroupId(), secretSantas.get(i).getID(),
                        secretSantas.get(i + 1).getID());
                Log.d(TAG, drawResult.toString());
                drawResults.add(drawResult);
            }
            DrawResult drawResult = new DrawResult(secretSantas.get(secretSantas.size() - 1).getGroupId(), secretSantas.get(secretSantas.size() - 1).getID(),
                    firstSecretSanta.getID());
            Log.d(TAG, drawResult.toString());
            drawResults.add(drawResult);
        }

        DrawResult.save(getApplicationContext(), drawResults);
        return secretSantas;
    }

    private void setupNetworkChangeListener() {
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mNetworkReceiver = new BroadcastReceiver() {
            @Override public void onReceive(Context context, Intent intent) {
                if (NetworkUtility.isConnectedToNetwork(context)) {
                    hideNoNetworkSnackBar();
                } else {
                    showNoNetworkSnackbar();
                }
            }
        };
        registerReceiver(mNetworkReceiver, intentFilter);
    }
}