package com.pandiandcode.perrankana.esecretsantapp.model.mediator;

import android.content.Context;

import com.pandiandcode.perrankana.esecretsantapp.model.DataRequest;
import com.pandiandcode.perrankana.esecretsantapp.model.DataResponse;
import com.pandiandcode.perrankana.esecretsantapp.model.mediator.webdata.ServiceProxy;
import com.pandiandcode.perrankana.esecretsantapp.utilities.Constants;

import retrofit.RestAdapter;

/**
 * Created by Perrankana on 28/11/15.
 */
public class DataMediator {

    private ServiceProxy mServiceProxy;

    public DataMediator(Context context){

        // Initialize the ServiceProxy.
        mServiceProxy =  new RestAdapter
                .Builder()
                .setEndpoint(Constants.SERVER_URL)
                .build()
                .create(ServiceProxy.class);
    }

    public DataResponse sendEmails(DataRequest dataRequest){
        try {
            return mServiceProxy.sendEmails(dataRequest);
        }catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }
}
