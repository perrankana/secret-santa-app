package com.pandiandcode.perrankana.esecretsantapp.task;

import android.content.Context;
import android.os.AsyncTask;

import com.pandiandcode.perrankana.esecretsantapp.model.DataRequest;
import com.pandiandcode.perrankana.esecretsantapp.model.DataResponse;
import com.pandiandcode.perrankana.esecretsantapp.model.SecretSanta;
import com.pandiandcode.perrankana.esecretsantapp.model.mediator.DataMediator;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Rocio Ortega on 04/12/2016.
 */

public class SendEmailsTask extends AsyncTask<String, Void, DataResponse> {

    private Context mContext;
    private String emailContent;
    private ArrayList<SecretSanta> mSecretSantas;
    private SendEmailsCallback mCallback;

    public SendEmailsTask(Context context, String emailContent, ArrayList<SecretSanta> secretSantas, SendEmailsCallback
            callback) {
        mContext = context;
        this.emailContent = emailContent;
        mSecretSantas = secretSantas;
        mCallback = callback;
    }

    @Override protected void onPreExecute() {
        super.onPreExecute();
        if (mCallback != null) {
            mCallback.doBefore();
        }
    }

    protected DataResponse doInBackground(String... urls) {

        DataRequest dataRequest = new DataRequest();
        dataRequest.setLang(Locale.getDefault().getLanguage());
        dataRequest.setContent(emailContent);
        dataRequest.setEmailslist(mSecretSantas);
        return new DataMediator(mContext).sendEmails(dataRequest);
    }

    protected void onPostExecute(DataResponse response) {
        if (mCallback != null) {
            if (response != null && response.getSuccess()) {
                mCallback.onSuccess();
            } else {
                mCallback.onFailed();
            }
        }


    }

    public interface SendEmailsCallback {
        public void doBefore();

        public void onSuccess();

        public void onFailed();
    }
}
