package com.pandiandcode.perrankana.esecretsantapp.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import com.pandiandcode.perrankana.esecretsantapp.persistance.DataBaseOpenHelper;

import java.util.ArrayList;

/**
 * Created by Rocio Ortega on 04/12/2016.
 */

public class Group {
    private int id;
    private String name;

    public Group(String name) {
        this.name = name;
    }

    public Group(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Group load(Context context, int id) {
        Group group = null;
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_GROUP,
                DataBaseOpenHelper.groupColumns, DataBaseOpenHelper.GROUP_ID + "=?",
                new String[]{id + ""}, null, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    group = new Group(cursor.getInt(0), cursor.getString(1));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return group;
    }

    public static ArrayList<Group> load(Context context) {
        ArrayList<Group> groups = new ArrayList<>();
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        Cursor cursor = mDbHelper.getWritableDatabase().query(DataBaseOpenHelper.TABLE_GROUP,
                DataBaseOpenHelper.groupColumns, null, new String[]{}, null, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    groups.add(new Group(cursor.getInt(0), cursor.getString(1)));
                } while (cursor.moveToNext());
            }
        }
        mDbHelper.getWritableDatabase().close();
        return groups;
    }

    public boolean save(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.GROUP_NAME, this.name);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().insert(DataBaseOpenHelper.TABLE_GROUP, null, values)) > 0;
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean delete(Context context) {
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().delete(DataBaseOpenHelper.TABLE_GROUP,
                DataBaseOpenHelper.GROUP_ID + "=?",
                new String[]{this.id + ""}) > 0);
        mDbHelper.getWritableDatabase().close();
        return saved;
    }

    public boolean update(Context context) {
        ContentValues values = new ContentValues();

        values.put(DataBaseOpenHelper.SECRETSANTA_NAME, this.name);
        DataBaseOpenHelper mDbHelper = new DataBaseOpenHelper(context);
        boolean saved = (mDbHelper.getWritableDatabase().update(DataBaseOpenHelper.TABLE_GROUP, values,
                DataBaseOpenHelper.GROUP_ID + "=?",
                new String[]{this.id + ""}) > 0);

        mDbHelper.getWritableDatabase().close();
        return saved;
    }
}
