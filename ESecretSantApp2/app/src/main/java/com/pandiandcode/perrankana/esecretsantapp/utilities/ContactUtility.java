package com.pandiandcode.perrankana.esecretsantapp.utilities;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import com.pandiandcode.perrankana.esecretsantapp.model.SecretSanta;

import java.util.Random;

/**
 * Created by Rocio Ortega on 20/11/2016.
 */

public class ContactUtility {

    public static int PICK_CONTACT_REQUEST_CODE = 1;

    public static Intent createPickContactIntent() {
        return new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Email.CONTENT_URI);
    }

    public static SecretSanta getSecretSantaFromContacts(ContentResolver contentResolver, Intent data) {

        Uri uri = data.getData();
        String[] projection = {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME, ContactsContract.CommonDataKinds.Email.DATA};

        Cursor cursor = contentResolver.query(uri, projection,
                null, null, null);
        if (cursor.moveToFirst()) {

            String name = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            Log.e("Name :", name);
            String email = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
            Log.e("Email", email);
            return new SecretSanta(name, email, (new Random()).nextInt(4), 0);
        }
        return null;
    }
}
