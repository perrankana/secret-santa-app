package com.pandiandcode.perrankana.esecretsantapp.ui.bindingAdapters;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

/**
 * Created by Rocio Ortega on 15/10/2016.
 */

public class ImageAdapter {

    @BindingAdapter({"android:src"})
    public static void setImageViewResource(ImageView imageView, int resource) {
        imageView.setImageResource(resource);
    }
}
